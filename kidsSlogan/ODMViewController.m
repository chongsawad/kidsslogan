//
//  ODMViewController.m
//  kidsSlogan
//
//  Created by InICe on 1/11/13.
//  Copyright (c) 2013 opendream. All rights reserved.
//

#import "ODMViewController.h"
#import "JSONKit.h"

@interface ODMViewController ()

@end

@implementation ODMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"kid" ofType:@"json"]];
    
    NSMutableArray *datasource = [[NSMutableArray alloc] initWithArray:[data objectFromJSONData]];
    
    NSMutableDictionary *resultDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *yearDictWithTextWrap = [[NSMutableDictionary alloc] init];
    
    
    CGRect frame = self.view.bounds;
    UIScrollView *contentView = [UIScrollView new];
    frame.size.width -= 100;
    [contentView setFrame:frame];
    [contentView setContentSize:CGSizeMake(9999, 9999)];
    [self.view addSubview:contentView];
    
    //CGFloat width = ( frame.size.width - 100 ) / 186;
    
    for (int count = 0; count < datasource.count; count++) { // for each item
        
        NSMutableDictionary *inputDict = [[NSMutableDictionary alloc] initWithDictionary:[datasource objectAtIndex:count]];
        
        NSString *hwlloWord = [inputDict objectForKey:@"text"];
        NSMutableArray *charArray = [NSMutableArray array];
        [hwlloWord enumerateSubstringsInRange:NSMakeRange(0, [hwlloWord length])
                                      options:NSStringEnumerationByWords
                                   usingBlock:^(NSString *substring,
                                                NSRange substringRange,
                                                NSRange enclosingRange,
                                                BOOL *stop)
        {
            [charArray addObject:substring];
        }];
        
        NSMutableArray *textArray = [NSMutableArray new];
        for (int i = 0; i < charArray.count; i++) {
            NSString *text = [charArray objectAtIndex:i];
            NSString *temp =[NSString stringWithUTF8String:[text cStringUsingEncoding:NSUTF8StringEncoding]];
            NSMutableArray *array = [resultDict objectForKey:temp];
            if (!array) {
                array = [[NSMutableArray alloc] init];
            }
            BOOL cont = NO;
            for (NSArray *detail in array) {
                if ([[detail objectAtIndex:0] isEqualToString:[inputDict objectForKey:@"year"]]) {
                    cont = YES;
                }
            }
            if (cont == NO) {
             
                NSMutableArray *arrayObj = [[NSMutableArray alloc] init];
                [arrayObj addObject:[inputDict objectForKey:@"year"]];
                [arrayObj addObject:@(i)];
                
                [array addObject:arrayObj];
            }
            
            [resultDict setObject:array forKey:temp];
            
            [textArray addObject:temp];
        }
        [inputDict setObject:textArray forKey:@"text_wrap"];
        [datasource replaceObjectAtIndex:count withObject:inputDict];
    }
    
//    CGFloat sumWidth = 0;
//    for (NSDictionary *dict in resultDict) {
//        NSString *word = [dict objectForKey:@"text"];
//        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(sumWidth, 0, 20, 20)];
//        textLabel.text = word;
//        textLabel.backgroundColor = [UIColor lightGrayColor];
//        [contentView addSubview:textLabel];
//    }
    
    NSLog(@"JSON %@",[resultDict JSONString]);
    
    NSLog(@"count x %i", resultDict.allKeys.count);
    //    NSError *error = nil;
    //    
    //    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    //    
    //    NSLog(@"Yeahh %i",[[resultDict JSONString] writeToFile:[docPath stringByAppendingPathComponent:@"results.json"] atomically:YES encoding:NSUTF8StringEncoding error:&error]);
    //    NSLog(@"error %@", error);
    
    NSError *error = nil;

    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];

    NSLog(@"Yeahh %i",[[resultDict JSONString] writeToFile:[docPath stringByAppendingPathComponent:@"results2.json"] atomically:YES encoding:NSUTF8StringEncoding error:&error]);
    NSLog(@"error %@", error);
}

@end

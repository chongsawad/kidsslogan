//
//  main.m
//  kidsSlogan
//
//  Created by InICe on 1/11/13.
//  Copyright (c) 2013 opendream. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ODMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ODMAppDelegate class]));
    }
}

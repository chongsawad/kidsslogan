//
//  ODMAppDelegate.h
//  kidsSlogan
//
//  Created by InICe on 1/11/13.
//  Copyright (c) 2013 opendream. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ODMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
